/**
 * @author Pekka Kuuva <pekka.kuuva@gmail.com>
 * @date 2019-11-15
 * @version 1.0
 * 
 * @specification: https://pekka.kuuva.gitlab.io/ncs/NCS-tech-problem.pdf
 */
// Tested on Chrome Version 78.0.3904.97

// Link Stations in form [xCoord, yCoord, reach]
/** @global */
var linkdata =  [
	[0,0,10],
	[20,20,5],
	[10,0,12]];

// Metering Points for Link Station Power in form [xCoord, yCoord]
/** @global */
var points = [
	[0,0],
	[100,100],
	[15,10],
	[18,18]];

// Loops through "points" and calls to print best link for each
function main(){  
  for (let i = 0; i < points.length; i++) {
    printBestLinkAt(points[i][0],points[i][1]);
  }
}

// Prints and Calls for strongest link, if not found link = 0.
function printBestLinkAt(x,y){
    let link = getStrongestLinkAt(x,y);
    if(link == 0){
    	document.getElementById("print").innerHTML += 
        "No link station within reach for point "+ x +","+ y +"<BR>";
    }
    else document.getElementById("print").innerHTML += 
      "Best link station for point "+ x +","+ y +" is "+
      link.xLocation +","+ link.yLocation +" with power "+ link.power.toFixed(3)
      +"<BR>";
}

// Function loops through links in "linkdata" and returns strongest link.
function getStrongestLinkAt(x,y){
  let power = 0, tempLink=0, bestLink=0;
  
  for (let i = 0; i < linkdata.length; i++) {
    tempLink = new Link(linkdata[i][0],linkdata[i][1],linkdata[i][2]);
    tempLink.power = tempLink.powerAtPoint(x,y);
    
    if (power < tempLink.power){
      power = tempLink.power;
      bestLink = tempLink; 
    }
  }
  return bestLink;
}

/** Class representing a link station. */
class Link {
  constructor(x, y, r) {
    this.xLocation = x;
    this.yLocation = y;
    this.reach = r;
    this.power = 0;
  }
  /**
   * Calculates power of this Link at given coordinates.
   * @param {xCoord, yCoord} Numbers
   * @return {}Number
   */  
  powerAtPoint(xCoord, yCoord) {
    let distanceToLink = Math.sqrt((xCoord-this.xLocation)**2 + (yCoord-this.yLocation)**2);    
    if (distanceToLink > this.reach) {return 0;}
    else return (this.reach - distanceToLink)**2;
  }
}